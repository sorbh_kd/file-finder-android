package unicode.labs.filefinder;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    private Context context;

    EditText fileName;
    Button scanBtn;
    TextView filePath;

    String mFilePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        fileName = (EditText) findViewById(R.id.fileName);
        scanBtn = (Button) findViewById(R.id.scanBtn);
        filePath = (TextView) findViewById(R.id.filePath);

        scanBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scanBtn:
                if (fileName.getText().toString().trim().length() > 0) {
                    String mFilePath = getFilePath(null, fileName.getText().toString().trim());
                    filePath.setText(mFilePath);
                } else {
                    fileName.setError("Please enter correct file name !!");
                }
                break;
        }
    }

    /**
     * Helper method to get file path
     */
    ProgressDialog pd;

    public String getFilePath(File mRootDir, String mFileName) {

        //Show dialog box as this process is going to take time
        pd = new ProgressDialog(context);
        pd.setMessage("Scaning SD card");
        pd.setCancelable(false);
        pd.show();

        //Search for this file * dont for get to add permission in android manifest file
        if (mRootDir == null)
            mRootDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        //Check the file path
        scanDir(mRootDir, mFileName);

        //Remove the processing dialog.
        if (pd != null)
            pd.dismiss();

        return mFilePath;
    }

    /**
     * Method to check the file inside a folder, pass the root folder
     * and this method will navigate through all the sub directory available inside
     *
     * @param mDir
     * @param mFileName
     * @return
     */
    public void scanDir(File mDir, String mFileName) {
//        Log.d(TAG, "Seraching for File : "+mFileName+" In root folder : "+mDir.getName());
        for (File f : mDir.listFiles()) {

            //Check if current file is File
            if (f.isFile()) {
//                Log.d(TAG, "       Matching With File : " + f.getName()+"  "+mFileName);
                if (f.getName().contentEquals(mFileName)) {
                    Log.d(TAG, "Match Found");
                    mFilePath = f.getPath();
                }
            }

            //Check for current file is dir and check the file inside it.
            if (f.isDirectory()) {
//                Log.d(TAG, "Checking in Dir : " + f.getName());
                scanDir(f, mFileName);
            }
        }
    }


}
